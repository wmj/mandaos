/*
 * Copyright (c) 2018 wmj
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the “Software”), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software
 * is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT
 * OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

package wmj.listacompras;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.db.framework.FrameworkSQLiteOpenHelperFactory;
import android.arch.persistence.room.testing.MigrationTestHelper;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;

import wmj.listacompras.db.ShopListDatabase;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static wmj.listacompras.db.ShopListDatabase.MIGRATION_1_2;

@RunWith(AndroidJUnit4.class)
public class MigrationTest {

    private static final String TEST_DB = "migration-test";

    @Rule
    public MigrationTestHelper helper;

    public MigrationTest() {
        helper = new MigrationTestHelper(
                InstrumentationRegistry.getInstrumentation(),
                ShopListDatabase.class.getCanonicalName(),
                new FrameworkSQLiteOpenHelperFactory()
        );
    }

    @Test
    public void migrate1To2() throws IOException {
        SupportSQLiteDatabase db = helper.createDatabase(TEST_DB, 1);

        db.execSQL("INSERT INTO category(id, name) VALUES(1, 'cat')");
        db.execSQL("INSERT INTO product(id, name, category_id, frequency) VALUES(1, 'cosa', 1, 0)");
        db.execSQL("INSERT INTO annotation(product_id, selected, timestamp) VALUEs(1, 0, 0)");

        db.close();

        db = helper.runMigrationsAndValidate(TEST_DB, 2, true, MIGRATION_1_2);

        Cursor c = db.query("SELECT COUNT(*) FROM annotation WHERE list_id = 1");
        assertTrue(c.moveToFirst());
        assertEquals(c.getInt(0), 1);
    }
}
