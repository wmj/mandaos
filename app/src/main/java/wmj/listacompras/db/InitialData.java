/*
 * Copyright (c) 2018 wmj
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the “Software”), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software
 * is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT
 * OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

package wmj.listacompras.db;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import wmj.listacompras.db.entities.table.CategoryEntity;
import wmj.listacompras.db.entities.table.ProductEntity;

public class InitialData {

    public static Map<CategoryEntity, List<ProductEntity>> generate(){
        final Map<String, String[]> data = new HashMap<String, String[]>() {
            {
                put("frutería", new String[]{
                        "naranjas",
                        "tomates",
                        "pimientos",
                });
                put("pescadería", new String[]{
                        "boquerones",
                        "sardinas",
                        "salmón",
                        "caballa",
                        "mejillones",
                        "almejas",
                        "gambas",
                        "langostinos",
                        "merluza",
                        "besugo",
                });
                put("limpieza", new String[]{
                        "lejía",
                        "detergente vajilla",
                        "detergente lavadora",
                        "suavizante",
                        "lejía lavadora",
                        "limpiador de baños",
                });
                put("bebidas", new String[]{
                        "agua",
                        "cerveza",
                        "vino tinto",
                        "vino blanco",
                        "zumo",
                        "tónica",
                        "batido",
                });
                put("carnicería", new String[]{
                        "chorizo",
                        "salchichón",
                        "morcilla",
                        "mortadela",
                        "pechuga de pollo",
                        "ternera",
                        "chuletas de cerdo",
                        "muslos de pollo",
                });
                put("", new String[]{
                        "destornillador",
                        "disco duro",
                });
            }
        };

        final Map<CategoryEntity, List<ProductEntity>> out = new HashMap<>();

        int idxCat = 1;
        //int idxProd = 1;

        for(final Map.Entry<String, String[]> cat: data.entrySet()){
            CategoryEntity category;
            if(cat.getKey().isEmpty()) {
                category = CategoryEntity.rootCategory();
            }else{
                final int newIdCat = idxCat++;
                category = new CategoryEntity(){{
                    id = newIdCat;
                    name = cat.getKey();
                }};
            }
            final List<ProductEntity> group = new ArrayList<>();
            for (final String n_prod: cat.getValue()) {
                group.add(new ProductEntity(){{
                    name = n_prod;
                    categoryId = category.id;
                }});
            }
            out.put(category, group);
        }

        return out;
    }
}
