/*
 * Copyright (c) 2018 wmj
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the “Software”), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software
 * is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT
 * OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

package wmj.listacompras.db;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.migration.Migration;
import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import wmj.listacompras.AppExecutors;
import wmj.listacompras.R;
import wmj.listacompras.db.dao.AnnotationDao;
import wmj.listacompras.db.dao.CategoryDao;
import wmj.listacompras.db.dao.ElementDao;
import wmj.listacompras.db.dao.ItemDao;
import wmj.listacompras.db.dao.ShopListDao;
import wmj.listacompras.db.dao.ProductDao;
import wmj.listacompras.db.entities.table.AnnotationEntity;
import wmj.listacompras.db.entities.table.CategoryEntity;
import wmj.listacompras.db.entities.table.ShopListEntity;
import wmj.listacompras.db.entities.table.ProductEntity;
import wmj.listacompras.file.ShopListFileParser;

@Database(entities = {
        CategoryEntity.class,
        ProductEntity.class,
        AnnotationEntity.class,
        ShopListEntity.class
}, version = 2)
public abstract class ShopListDatabase extends RoomDatabase {

    @VisibleForTesting
    public static final String DATABASE_NAME = "shoplist_database";

    public abstract CategoryDao categoryDao();

    public abstract ProductDao productDao();

    public abstract AnnotationDao annotationDao();

    public abstract ElementDao elementDao();

    public abstract ItemDao itemDao();

    public abstract ShopListDao shopListDao();

    private static ShopListDatabase INSTANCE;

    public static ShopListDatabase getInstance(
        final Context context,
        final AppExecutors executors
    )
    {
        if (INSTANCE == null) {
            synchronized (ShopListDatabase.class) {
                if (INSTANCE == null) {
                    // Create database here
                    INSTANCE = buildDatabase(context, executors);
                }
            }
        }
        return INSTANCE;
    }

    @NonNull
    private static ShopListDatabase buildDatabase(
        final Context context,
        final AppExecutors executors
    )
    {
        return Room.databaseBuilder(
                context.getApplicationContext(),
                ShopListDatabase.class,
                DATABASE_NAME
        ).addCallback(new Callback() {
            @Override
            public void onCreate(@NonNull SupportSQLiteDatabase db) {
                super.onCreate(db);
                executors.diskExecutor().execute(() -> {
                    try {
                        Thread.sleep(4000);
                    } catch (InterruptedException e) {
                        //e.printStackTrace();
                    }
                    initSqliteDatabase(db);
                    ShopListDatabase shopDb = ShopListDatabase.getInstance(context, executors);
                    ShopListDatabase.initDatabase(shopDb, context);
//                    ShopListDatabase.prePopulate(shopDb);
                    try {
                        shopDb.importFromResources(context);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
            }
        }).addMigrations(MIGRATION_1_2).build();
    }

    private static void initSqliteDatabase(@NonNull final SupportSQLiteDatabase db) { }

    private static void initDatabase(@NonNull final ShopListDatabase db, @NonNull final Context context) {
        final String name = context.getString(R.string.main);
        db.shopListDao().insert(ShopListEntity.newList(name));
    }

//    private static void prePopulate(@NonNull final ShopListDatabase db){
//        CategoryDao catDao = db.categoryDao();
//        ProductDao prodDao = db.productDao();
//        Map<CategoryEntity, List<ProductEntity>> data = InitialData.generate();
//        db.runInTransaction(() -> {
//            CategoryEntity emptyCat = null;
//            for(CategoryEntity cat: data.keySet()){
//                if(cat.isRoot()){
//                    emptyCat = cat;
//                    break;
//                }
//            }
//            if(emptyCat != null){
//                List<ProductEntity> orphans = data.get(emptyCat);
//                prodDao.insertAll(orphans);
//                data.remove(emptyCat);
//            }
//            catDao.insertAll(data.keySet());
//            for(List<ProductEntity> group: data.values()){
//                prodDao.insertAll(group);
//            }
//        });
//    }

    private void importFromResources(@NonNull final Context context) throws IOException {
        try(final InputStream stream = context.getResources().openRawResource(R.raw.articles)){
            this.beginTransaction();
            try {
                this.importFromStream(stream);
                this.setTransactionSuccessful();
            } finally {
               this.endTransaction();
            }
        }
    }

    public void importFromStream(@NonNull final InputStream stream) throws IOException {
        final CategoryDao categoryDao = this.categoryDao();
        final ProductDao productDao = this.productDao();
        ShopListFileParser reader = new ShopListFileParser(new ShopListFileParser.Listener() {
            @Override
            public void onNewCategory(@NonNull CategoryEntity category) {
                categoryDao.insert(category);
            }

            @Override
            public void onNewProductList(@NonNull List<ProductEntity> products) {
                productDao.insertAll(products);
            }
        });
        reader.parse(stream);
    }

    public static final Migration MIGRATION_1_2 = new Migration(1, 2) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {

            database.beginTransaction();
            try {
                // shoplist
                database.execSQL(
                        "CREATE TABLE IF NOT EXISTS `shop_list` ("
                                + "`id` INTEGER PRIMARY KEY AUTOINCREMENT, "
                                + "`name` TEXT NOT NULL)"
                );
                database.execSQL(
                        "CREATE UNIQUE INDEX `index_shop_list_name` ON `shop_list` (`name`)"
                );

                // default list
                ContentValues row = new ContentValues();
                row.put("id", 1);
                row.put("name", "main");

                database.insert("shop_list", SQLiteDatabase.CONFLICT_FAIL, row);

                // PRAGMA foreign_keys=OFF
                database.execSQL("PRAGMA foreign_keys=OFF");

                // create table new
                database.execSQL(
                        "CREATE TABLE `NEW_annotation` ("
                                + "`product_id` INTEGER NOT NULL, "
                                + "`list_id` INTEGER NOT NULL, "
                                + "`selected` INTEGER NOT NULL, "
                                + "`timestamp` INTEGER NOT NULL, "
                                + "`details` TEXT, "
                                + "PRIMARY KEY(`product_id`, `list_id`), "
                                + "FOREIGN KEY(`product_id`) REFERENCES `product`(`id`) ON UPDATE NO ACTION ON DELETE CASCADE , "
                                + "FOREIGN KEY(`list_id`) REFERENCES `shop_list`(`id`) ON UPDATE NO ACTION ON DELETE CASCADE )"
                );

                // insert old into new
                database.execSQL(
                        "INSERT INTO NEW_annotation(product_id, selected, timestamp, details, list_id)"
                                + " SELECT product_id, selected, timestamp, details, 1"
                                + " FROM annotation"
                );
                // drop table
                database.execSQL("DROP TABLE annotation");

                // alter table new rename to old
                database.execSQL("ALTER TABLE NEW_annotation RENAME TO annotation");

                // PRAGMA foreign_key_check
                database.execSQL("PRAGMA foreign_key_check");

                // PRAGMA foreign_keys=ON
                database.execSQL("PRAGMA foreign_keys=ON");

                database.setTransactionSuccessful();
            }catch(Exception e){
                e.printStackTrace();;
            }finally{
                database.endTransaction();
            }
        }
    };
}
