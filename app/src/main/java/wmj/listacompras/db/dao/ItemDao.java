/*
 * Copyright (c) 2018 wmj
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the “Software”), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software
 * is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT
 * OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

package wmj.listacompras.db.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;

import java.util.List;

import wmj.listacompras.db.entities.AnnotationInfo;
import wmj.listacompras.db.entities.table.CategoryEntity;

/*
SELECT a.*, p.name, p.category_id AS categoryId, c.name AS categoryName, (p.category_id = 0) AS inRoot,
 FROM annotation a JOIN product p ON a.product_id = p.id
 JOIN category c ON p.category_id = c.id
 ORDER BY a.selected, inRoot, c.name COLLATE NOCASE, p.category_id, p.name COLLATE NOCASE
 */
@Dao
public interface ItemDao {

    @Query(
        "SELECT a.*, p.name, p.category_id AS categoryId, c.name AS categoryName,"
            + " (p.category_id = " + CategoryEntity.ROOT_CATEGORY_ID +") AS inRoot"
            +" FROM annotation a JOIN product p ON a.product_id = p.id"
            +" JOIN category c ON p.category_id = c.id"
            +" WHERE a.list_id = :listId"
            +" ORDER BY a.selected, inRoot, c.name COLLATE NOCASE, p.category_id, p.name COLLATE NOCASE"
    )
    LiveData<List<AnnotationInfo>> selectFromList(int listId);

}
