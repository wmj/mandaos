/*
 * Copyright (c) 2018 wmj
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the “Software”), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software
 * is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT
 * OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

package wmj.listacompras.db.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.Collection;
import java.util.List;

import wmj.listacompras.db.entities.table.ProductEntity;

@Dao
public interface ProductDao {

    @Insert
    void insert(ProductEntity product);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertOrIgnore(ProductEntity product);

    @Insert
    void insertAll(Collection<ProductEntity> products);

    @Query("SELECT * FROM product WHERE id = :productId")
    ProductEntity select(int productId);

    @Query("SELECT * FROM product")
    LiveData<List<ProductEntity>> selectAll();

    @Query("DELETE FROM product WHERE id = :productId")
    void delete(int productId);

    @Update
    void update(ProductEntity product);

    @Query("UPDATE product SET frequency = IFNULL(frequency, 0)+1 WHERE id = :productId")
    void incrementFrequency(int productId);

    @Query("UPDATE product SET frequency = frequency-1"
               +" WHERE id = :productId AND frequency IS NOT NULL AND frequency > 0")
    void decrementFrequency(int productId);

    @Query("UPDATE product SET frequency = 0 WHERE frequency > 0")
    void resetHistory();

    @Query("UPDATE product SET frequency = 0 WHERE id = :productId AND frequency > 0")
    void resetFrequency(int productId);
}
