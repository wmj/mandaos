/*
 * Copyright (c) 2018 wmj
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the “Software”), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software
 * is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT
 * OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

package wmj.listacompras.db.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import wmj.listacompras.db.ShopListInfo;
import wmj.listacompras.db.entities.table.ShopListEntity;

@Dao
public interface ShopListDao {

    @Insert
    void insert(ShopListEntity list);

//    @Query("SELECT * FROM shop_list")
//    LiveData<List<ShopListEntity>> selectAll();

    @Query(
        "SELECT l.*, a.items, a.lastTimestamp FROM shop_list l LEFT JOIN "
        +"(SELECT list_id, COUNT(*) AS items, MAX(timestamp) AS lastTimestamp "
        +" FROM annotation WHERE selected < 1 GROUP BY list_id) AS a"
        +" ON l.id = a.list_id"
    )
    LiveData<List<ShopListInfo>> selectAll();

    @Query("SELECT * FROM shop_list WHERE id = :listId")
    LiveData<ShopListEntity> select(int listId);

    @Query("SELECT * FROM shop_list WHERE id = :listId")
    ShopListEntity get(int listId);

    @Query("SELECT * FROM shop_list LIMIT 1")
    ShopListEntity first();

    @Update
    int update(ShopListEntity item);

    @Query("DELETE FROM shop_list WHERE id = :listId")
    void delete(int listId);
}
