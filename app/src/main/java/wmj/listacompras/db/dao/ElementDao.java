/*
 * Copyright (c) 2018 wmj
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the “Software”), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software
 * is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT
 * OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

package wmj.listacompras.db.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;

import java.util.List;

import wmj.listacompras.db.entities.Element;
import wmj.listacompras.db.entities.ElementInfo;
import wmj.listacompras.db.entities.ElementWithCategory;
import wmj.listacompras.db.entities.table.CategoryEntity;

@Dao
public interface ElementDao {

    String COLS_ANNOTATED_PRODUCT = "p.id, p.name, p.category_id AS categoryId, 0 AS type, IFNULL(NOT a.selected, 0) AS annotated";

    String SELECT_ANNOTATED_PRODUCT =
        "SELECT "+ COLS_ANNOTATED_PRODUCT + " FROM product p"
            + " LEFT JOIN (SELECT * FROM annotation WHERE list_id = :listId) a"
            + " ON p.id = a.product_id";

    String SELECT_ANNOTATED_PRODCAT =
        "SELECT "+ COLS_ANNOTATED_PRODUCT + ", c.name AS categoryName, p.frequency FROM product p"
            + " JOIN category c ON p.category_id = c.id"
            + " LEFT JOIN (SELECT * FROM annotation WHERE list_id = :listId) a"
            + " ON p.id = a.product_id";

    String SELECT_CATEGORY =
        "SELECT id, name, parent_id AS categoryId, 1 AS type, 0 AS annotated FROM category";


    @Query(
        "SELECT * FROM ("
            + SELECT_CATEGORY
            + " WHERE parent_id = :categoryId"
            + " UNION "
            + SELECT_ANNOTATED_PRODUCT
            + " WHERE p.category_id = :categoryId"
            +") ORDER BY type DESC, name COLLATE NOCASE, id")
    LiveData<List<ElementInfo>> selectSiblings(int listId, int categoryId);

    @Query(SELECT_ANNOTATED_PRODCAT
            + " WHERE p.frequency > 0 ORDER BY p.frequency DESC, p.name LIMIT 50")
    LiveData<List<ElementWithCategory>> selectHistory(int listId);

    @Query(
        "SELECT id, name, category_id AS categoryId, 0 AS type FROM product"+
            " UNION "+
            "SELECT id, name, parent_id AS categoryId, 1 AS type FROM category"+
            " WHERE id != " + CategoryEntity.ROOT_CATEGORY_ID
    )
    List<Element> selectAll();
}
