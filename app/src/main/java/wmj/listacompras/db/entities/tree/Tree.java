/*
 * Copyright (c) 2018 wmj
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the “Software”), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software
 * is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT
 * OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

package wmj.listacompras.db.entities.tree;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import wmj.listacompras.db.entities.Element;
import wmj.listacompras.db.entities.table.CategoryEntity;

public class Tree<T extends Element> {

    public final T item;

    public Tree(T item) {
        this.item = item;
    }

    public final List<T> products = new ArrayList<>();

    public final List<Tree<T>> categories = new ArrayList<>();

    @Override
    public String toString(){
        return String.format("<%s<%s>>",
            this.getClass().getSimpleName(),
            item.toString()
        );
    }

    public static Tree<Element> build(@NonNull final List<Element> nodes) {

        final TreeHydrator<Element> hydrator = new TreeHydrator<>(nodes);

        // tree
        return hydrator.hydrate(new Element() {{
            id = CategoryEntity.ROOT_CATEGORY_ID;
            type = Element.TYPE_CATEGORY;
        }});
    }

}
