/*
 * Copyright (c) 2018 wmj
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the “Software”), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software
 * is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT
 * OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

package wmj.listacompras.db.entities.table;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity(
    tableName = "category",
    indices = @Index(value={"name", "parent_id"}, unique=true),
    foreignKeys = @ForeignKey(
        entity = CategoryEntity.class,
        parentColumns = "id",
        childColumns = "parent_id",
        onDelete = ForeignKey.CASCADE
    )
)
public class CategoryEntity {

    public final static int ROOT_CATEGORY_ID = -1;
    public final static int ORPHAN_CATEGORY_ID = -2;

    @PrimaryKey(autoGenerate = true)
    public Integer id;

    @NonNull
    public String name;

    @ColumnInfo(name="parent_id")
    public Integer parentId = ROOT_CATEGORY_ID;

    public static CategoryEntity rootCategory() {
        CategoryEntity cat = new CategoryEntity() {{
            id = ROOT_CATEGORY_ID;
            name = "ROOT";
            parentId = null;
        }};
        return cat;
    }

    public boolean isRoot() {
        return id == ROOT_CATEGORY_ID;
    }

    @Override
    public String toString(){
        return String.format("CategoryEntity<%d, %s, %d>", id, name, parentId);
    }

}

