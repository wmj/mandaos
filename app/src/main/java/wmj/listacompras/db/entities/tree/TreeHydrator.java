/*
 * Copyright (c) 2018 wmj
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the “Software”), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software
 * is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT
 * OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

package wmj.listacompras.db.entities.tree;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import wmj.listacompras.db.entities.Element;

public class TreeHydrator<T extends Element> {
    private final Map<Integer, List<T>> table = new HashMap<>();

    public TreeHydrator(@NonNull final List<T> nodes) {
        // generating map
        for(final T node: nodes) {
            final int parentId = node.categoryId;
            List<T> siblings = table.get(parentId);
            if(siblings == null){
                siblings = new ArrayList<>();
                table.put(parentId, siblings);
            }
            siblings.add(node);
        }
    }

    public Tree<T> hydrate(@NonNull final T item){
        final Tree<T> node = new Tree<>(item);
        if(!item.isCategory()) {
            return node;
        }

        final List<T> children = table.get(node.item.id);
        if(children == null){
            return node;
        }
        children
            .stream()
            .map(this::hydrate)
            .forEach(x -> {
                if(x.item.isCategory()){
                    node.categories.add(x);
                }else{
                    node.products.add(x.item);
                }
            });

        return node;
    }
}
