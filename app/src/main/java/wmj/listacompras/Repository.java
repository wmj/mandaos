/*
 * Copyright (c) 2018 wmj
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the “Software”), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software
 * is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT
 * OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

package wmj.listacompras;

import android.arch.lifecycle.LiveData;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.BitSet;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import wmj.listacompras.db.ShopListDatabase;
import wmj.listacompras.db.ShopListInfo;
import wmj.listacompras.db.entities.AnnotationInfo;
import wmj.listacompras.db.entities.ChildCategory;
import wmj.listacompras.db.entities.Element;
import wmj.listacompras.db.entities.ElementInfo;
import wmj.listacompras.db.entities.ElementWithCategory;
import wmj.listacompras.db.entities.table.AnnotationEntity;
import wmj.listacompras.db.entities.table.CategoryEntity;
import wmj.listacompras.db.entities.table.ProductEntity;
import wmj.listacompras.db.entities.table.ShopListEntity;
import wmj.listacompras.db.entities.tree.Tree;
import wmj.listacompras.file.ShopListFileWriter;

public class Repository {

    private static Repository sInstance;

    private final ShopListDatabase mDatabase;

    private Repository(final ShopListDatabase database) {
        mDatabase = database;
    }

    public static Repository getInstance(final ShopListDatabase database) {
        if (sInstance == null) {
            synchronized (Repository.class) {
                if (sInstance == null){
                    sInstance = new Repository(database);
                }
            }
        }
        return sInstance;
    }

    //region product

    public void insertOrIgnoreProduct(ProductEntity product) {
        AppExecutors.diskExecutor().execute(() -> mDatabase.productDao().insertOrIgnore(product));
    }

    public Future<ProductEntity> selectFutureProduct(int productId) {
        return AppExecutors.diskExecutor().submit(
            () -> mDatabase.productDao().select(productId)
        );
    }

    public void updateProduct(ProductEntity product) {
        AppExecutors.diskExecutor().execute(() -> mDatabase.productDao().update(product));
    }

    public void deleteProduct(int productId) {
        AppExecutors.diskExecutor().execute(() -> mDatabase.productDao().delete(productId));
    }

    public void resetHistory() {
        AppExecutors.diskExecutor().execute(() -> mDatabase.productDao().resetHistory());
    }

    public void resetProductFrequency(int productId) {
        AppExecutors.diskExecutor().execute(() -> mDatabase.productDao().resetFrequency(productId));
    }

    //endregion

    //region category

    public void insertOrIgnoreCategory(CategoryEntity category) {
        AppExecutors.diskExecutor().execute(() -> mDatabase.categoryDao().insertOrIgnore(category));
    }

    public Future<CategoryEntity> selectFutureCategory(int categoryId) {
        return AppExecutors.diskExecutor().submit(
            () -> mDatabase.categoryDao().select(categoryId)
        );
    }

    public void updateCategory(CategoryEntity category) {
        AppExecutors.diskExecutor().execute(() -> mDatabase.categoryDao().update(category));
    }

    public void deleteCategory(int categoryId) {
        AppExecutors.diskExecutor().execute(() -> mDatabase.categoryDao().delete(categoryId));
    }

    public LiveData<ChildCategory> selectCategoryWithParent(int categoryId) {
        return mDatabase.categoryDao().selectWithParent(categoryId);
    }

    //endregion

    //region node

    public LiveData<List<ElementInfo>> listNodes(int listId, int categoryId) {
        return mDatabase.elementDao().selectSiblings(listId, categoryId);
    }

    public LiveData<List<ElementWithCategory>> listHistory(int listId) {
        return mDatabase.elementDao().selectHistory(listId);
    }

    //endregion

    //region annotation

    public void insertOrIgnoreAnnotation(@NonNull AnnotationEntity annotation) {
        AppExecutors.diskExecutor().execute(() -> mDatabase.annotationDao().insertOrIgnore(annotation));
    }

    private void insertOrReplace(@NonNull AnnotationEntity annotation) {
        AppExecutors.diskExecutor().execute(() -> mDatabase.annotationDao().insertOrReplace(annotation));
    }

    public void deleteAnnotation(int listId, int productId) {
        AppExecutors.diskExecutor().execute(() -> mDatabase.annotationDao().delete(listId, productId));
    }

    public LiveData<List<AnnotationInfo>> listItems(int shopListId) {
        return mDatabase.itemDao().selectFromList(shopListId);
    }

    public void updateAnnotation(@NonNull AnnotationEntity annotation) {
        AppExecutors.diskExecutor().execute(() -> mDatabase.annotationDao().update(annotation));
    }

    public LiveData<Integer> numberOfActiveAnnotations(int listId){
        return mDatabase.annotationDao().numberOfActiveAnnotations(listId);
    }

    public void useAnnotation(int listId, int productId, boolean selected) {
        AppExecutors.diskExecutor().execute(() -> mDatabase.runInTransaction(() -> {
            long timestamp = UnixTime.now();
            mDatabase.annotationDao().useAnnotation(listId, productId, selected, timestamp);
            if(selected){
                mDatabase.productDao().incrementFrequency(productId);
            }else{
                mDatabase.productDao().decrementFrequency(productId);
            }
        }));
    }

    public void removeOldAnnotations(int listId) {
        AppExecutors.diskExecutor().execute(() -> mDatabase.annotationDao().removeSelected(listId));
    }

    public void removeOldAnnotations(int listId, int hours) {
        long threshold = UnixTime.now() - UnixTime.delta(hours);
        AppExecutors.diskExecutor().execute(() -> mDatabase.annotationDao().removeOldSelected(listId, threshold));
    }

    public void removeAllOldAnnotations(int hours) {
        long threshold = UnixTime.now() - UnixTime.delta(hours);
        AppExecutors.diskExecutor().execute(() -> mDatabase.annotationDao().removeOldSelected(threshold));
    }

    public void addAnnotation(int listId, final ElementInfo item) {
        final AnnotationEntity annotation = item.newAnnotation(listId);
        if(item.annotated){
            insertOrIgnoreAnnotation(annotation);
        }else{
            insertOrReplace(annotation);
        }
    }

    //endregion

    //region shoplist

    public LiveData<List<ShopListInfo>> selectShopLists() {
        return mDatabase.shopListDao().selectAll();
    }

    public Future<ShopListEntity> selectShopList(int listId) {
        return AppExecutors.diskExecutor().submit(() -> mDatabase.shopListDao().get(listId));
    }

    //endregion

    //region import-export

    public Tree<Element> getTree() {
        return Tree.build(selectAllElements());
    }

    private List<Element> selectAllElements() {
        return mDatabase.elementDao().selectAll();
    }

    /**
     * TODO avoid main thread
     * exportDatabase the database as a json file
     * @param out
     * @throws IOException
     * @throws ExecutionException
     * @throws InterruptedException
     */
    public void exportDatabase(final OutputStream out)
        throws IOException
    {
        try(ShopListFileWriter writer = new ShopListFileWriter(out)) {
            writer.writeCategory(getTree());
        }
    }

    /**
     * TODO avoid main thread
     * TODO clear previous contents
     * import database from a json file
     * @param in
     * @throws IOException
     */
    public void importDatabase(final InputStream in) throws IOException {
        try {
            mDatabase.beginTransaction();
            mDatabase.categoryDao().deleteAll();
            mDatabase.importFromStream(in);
            mDatabase.setTransactionSuccessful();
        }catch(IOException e) {
            throw e;
        }finally{
            mDatabase.endTransaction();
        }
    }

    public ShopListEntity getDefaultShopList(Context context) {
        final SharedPreferences prefs = context.getSharedPreferences(
                context.getString(R.string.preference_status),
                Context.MODE_PRIVATE
        );
        final int listId = prefs.getInt(context.getString(R.string.preference_status_shoplist_id), -1);

        ShopListEntity item = null;
        try {
            item = AppExecutors
                    .diskExecutor()
                    .submit(() -> {
                        ShopListEntity output = null;

                        if(listId >= 0) {
                            output = mDatabase.shopListDao().get(listId);
                        }
                        if((listId < 0) || (output == null)){
                            output = mDatabase.shopListDao().first();
                        }
                        return output;
                    })
                    .get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return item;
    }

    public void updateShopList(ShopListEntity item) {
        AppExecutors.diskExecutor().execute(() -> mDatabase.shopListDao().update(item));
    }

    public void insertShopList(ShopListEntity shopList) {
        AppExecutors.diskExecutor().execute(() -> mDatabase.shopListDao().insert(shopList));
    }

    public void deleteShopList(int shopListId) {
        AppExecutors.diskExecutor().execute(() -> mDatabase.shopListDao().delete(shopListId));
    }

    //endregion

}
