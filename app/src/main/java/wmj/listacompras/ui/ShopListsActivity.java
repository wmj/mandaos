/*
 * Copyright (c) 2018 wmj
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the “Software”), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software
 * is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT
 * OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

package wmj.listacompras.ui;

import android.app.AlertDialog;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import java.util.concurrent.ExecutionException;

import wmj.listacompras.R;
import wmj.listacompras.db.entities.table.ProductEntity;
import wmj.listacompras.db.entities.table.ShopListEntity;
import wmj.listacompras.ui.dialog.EditProductDialog;
import wmj.listacompras.ui.dialog.EditShopListDialog;
import wmj.listacompras.ui.dialog.NewShopListDialog;
import wmj.listacompras.ui.shoplists.ShopListsFragment;
import wmj.listacompras.viewmodel.ShopListsViewModel;

public class ShopListsActivity
        extends AppCompatActivity
        implements ShopListsFragment.Listener
{
    private ShopListsViewModel mModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop_lists);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mModel = ViewModelProviders.of(this).get(ShopListsViewModel.class);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    //region ShopListsFragment.Listener

    @Override
    public void onEditShopList(@NonNull View view, @NonNull ShopListEntity item) {
        EditShopListDialog dialog = EditShopListDialog.newInstance(item.name);
        dialog.setEditShopListDialogListener(text -> {
            item.name = text;
            mModel.update(item);
        });
        dialog.show(getSupportFragmentManager(), "edit shoplist");
    }

    @Override
    public void onDeleteShopList(@NonNull View view, @NonNull ShopListEntity item) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder
                .setTitle(R.string.remove_shoplist)
                .setMessage(getResources().getString(R.string.remove_shoplist_message, item.name))
                .setNegativeButton(android.R.string.cancel, null)
                .setPositiveButton(android.R.string.ok, (dialog, which) -> mModel.deleteShopList(item.id))
                .show();
    }

    @Override
    public void onNewShopList() {
        NewShopListDialog dialog = new NewShopListDialog();
        dialog.setNewShopListDialogListener(text -> mModel.insert(new ShopListEntity() {{
            name = text;
        }}));
        dialog.show(getSupportFragmentManager(), "new shoplist");
    }

    //endregion
}
