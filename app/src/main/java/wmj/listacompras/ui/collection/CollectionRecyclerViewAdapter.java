/*
 * Copyright (c) 2018 wmj
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the “Software”), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software
 * is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT
 * OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

package wmj.listacompras.ui.collection;

import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import wmj.listacompras.R;
import wmj.listacompras.db.entities.Element;
import wmj.listacompras.db.entities.ElementInfo;
import wmj.listacompras.ui.listrecyclerview.AViewHolder;
import wmj.listacompras.ui.listrecyclerview.ListRecyclerViewAdapter;

/**
 * {@link RecyclerView.Adapter} that can display a {@link ElementInfo} and makes a call to the
 * specified {@link CollectionFragment.Listener}.
 */
public class CollectionRecyclerViewAdapter extends
    ListRecyclerViewAdapter<ElementInfo, CollectionRecyclerViewAdapter.NodeViewHolder, CollectionFragment.Listener>
{
    public CollectionRecyclerViewAdapter(CollectionFragment.Listener listener) {
        super(listener);
    }

    @Override
    public NodeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        Element.Type type = Element.Type.values()[viewType];

        View view;

        final LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());

        switch(type) {
            case PRODUCT:
                view = layoutInflater.inflate(R.layout.fragment_collection_item_product, parent, false);
                return new ProductViewHolder(view);
            //case CATEGORY:
            default:
                view = layoutInflater.inflate(R.layout.fragment_collection_item_category, parent, false);
                return new CategoryViewHolder(view);
        }
    }

    @Override
    public int getItemViewType(int position){
        return mValues.get(position).getType().ordinal();
    }

    //region ViewHolder

    public class NodeViewHolder extends AViewHolder<ElementInfo> {

//        public final View mView;
        public ElementInfo mItem;

        public NodeViewHolder(@NonNull View itemView) {
            super(itemView);
        }

        @CallSuper
        public void bind(final ElementInfo item, int position) {
            mItem = item;
        }
    }

    public class ProductViewHolder extends NodeViewHolder
        implements CompoundButton.OnCheckedChangeListener
    {
        public final CheckBox mContentView;

        public ProductViewHolder(@NonNull View view) {
            super(view);
            mContentView = view.findViewById(R.id.content);
        }

        @Override
        public void bind(final ElementInfo item, int position) {
            super.bind(item, position);
            mContentView.setText(mItem.name);

            setChecked(mItem.annotated);

            mContentView.setOnLongClickListener(view -> {
                if(null != mListener) {
                    mListener.onProductSecondaryAction(view, mItem);
                }
                return true;
            });
        }

        private void setChecked(boolean value) {
            mContentView.setOnCheckedChangeListener(null);
            mContentView.setChecked(value);
            mContentView.setOnCheckedChangeListener(this);
        }

        @Override
        public void onCheckedChanged(CompoundButton view, boolean isChecked) {
            if (null != mListener) {
                mListener.onProductSelected(view, mItem, isChecked);
            }
        }
    }

    public class CategoryViewHolder extends NodeViewHolder {

        public final TextView mContentView;

        public CategoryViewHolder(@NonNull View view) {
            super(view);
            mContentView = view.findViewById(R.id.content);
        }

        @Override
        public void bind(final ElementInfo item, int position) {
            super.bind(item, position);
            mContentView.setText(mItem.name);

            mContentView.setOnClickListener(view -> {
                if (null != mListener) {
                    mListener.onCategoryPrimaryAction(view, mItem);
                }
            });
            mContentView.setOnLongClickListener(view -> {
                if(null != mListener) {
                    mListener.onCategorySecondaryAction(view, mItem);
                }
                return true;
            });
        }
    }

    //endregion
}
