/*
 * Copyright (c) 2018 wmj
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the “Software”), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software
 * is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT
 * OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

package wmj.listacompras.ui.collection;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import wmj.listacompras.R;
import wmj.listacompras.db.entities.ChildCategory;
import wmj.listacompras.db.entities.table.CategoryEntity;
import wmj.listacompras.viewmodel.CollectionViewModel;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Listener} interface
 * to handle interaction events.
 */
public class CollectionHeaderFragment extends Fragment {

    private Listener mListener;

    private View mView;

    private CollectionViewModel mNodesModel;

    public CollectionHeaderFragment() {}

    @Override
    public View onCreateView(
        LayoutInflater inflater,
        ViewGroup container,
        Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_collection_navigation, container, false);
        return mView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mNodesModel = ViewModelProviders.of(getActivity()).get(CollectionViewModel.class);
        mNodesModel.getCategory().observe(this, this::bind);
    }

    private void bind(final ChildCategory category) {
        final TextView viewParent = mView.findViewById(R.id.parent_category);
        final TextView viewCategory = mView.findViewById(R.id.current_category);
//        final TextView viewSeparator = mView.findViewById(R.id.separator);

        if ((category==null) || category.current.isRoot()) {
            bindRoot(viewParent, viewCategory);
        } else {
            bindChild(category, viewParent, viewCategory);
        }
    }

    private void bindRoot(
        @NonNull final TextView viewParent,
        @NonNull final TextView viewCategory
    ) {
        viewCategory.setVisibility(View.GONE);
        setRootView(viewParent);
        viewParent.setOnClickListener(null);
    }

    private void bindChild(
        @NonNull final ChildCategory category,
        @NonNull final TextView viewParent,
        @NonNull final TextView viewCategory
    ) {
        // category
        viewCategory.setVisibility(View.VISIBLE);
        viewCategory.setText(category.current.name);

        // parent
        if(category.current.parentId == CategoryEntity.ROOT_CATEGORY_ID){
            setRootView(viewParent);
        } else {
            viewParent.setText(category.parentName);
            viewParent.setBackground(null);
        }
        viewParent.setOnClickListener(view -> {
            if (mListener != null) {
                mListener.onReturnToParentCategory(category);
            }
        });
    }

    private void setRootView(@NonNull TextView viewParent) {
        viewParent.setText(null);
        viewParent.setBackground(getResources().getDrawable(R.drawable.ic_breezeicons_actions_22_anchor, null));
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try{
            mListener = (Listener) context;
        }catch(ClassCastException e){
            throw new RuntimeException(context.toString()
                                           + " must implement CollectionHeaderFragment.Listener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface Listener {
        void onReturnToParentCategory(ChildCategory currentCategory);
    }
}
