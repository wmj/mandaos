/*
 * Copyright (c) 2018 wmj
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the “Software”), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software
 * is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT
 * OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

package wmj.listacompras.ui.collection;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import wmj.listacompras.R;


public class CollectionWrapperFragment
    extends Fragment
{
    private CollectionFragment.Listener mListener;
    private final int NUM_COLUMNS = 2;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(
        LayoutInflater inflater,
        ViewGroup container,
        Bundle savedInstanceState
    ) {
        final View view = inflater.inflate(R.layout.fragment_collection_wrapper, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (savedInstanceState == null){
            CollectionFragment fragment = CollectionFragment.newInstance(NUM_COLUMNS);
            getChildFragmentManager()
                .beginTransaction()
                .add(R.id.fragment_container_node_list, fragment)
                .commit();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try{
            mListener = (CollectionFragment.Listener) context;
        } catch(ClassCastException e) {
            throw new RuntimeException(context.toString()
                                           + " must implement CollectionFragment.Listener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.collection, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.menu_collection_new_product:
                requestNewProduct();
                return true;
            case R.id.menu_collection_new_category:
                requestNewCategory();
                return true;
            case R.id.menu_collection_import:
                importCollection();
                return true;
            case R.id.menu_collection_export:
                exportCollection();
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void exportCollection() {
        if(null != mListener){
            mListener.onExport();
        }
    }

    private void importCollection() {
        if(null != mListener){
            mListener.onImport();
        }
    }

    private void requestNewProduct(){
        final CollectionFragment fragment = getNodeListFragment();
        if(fragment != null){
            fragment.requestNewProduct();
        }
    }

    private void requestNewCategory(){
        final CollectionFragment fragment = getNodeListFragment();
        if(fragment != null){
            fragment.requestNewCategory();
        }
    }

    private CollectionFragment getNodeListFragment(){
        return (CollectionFragment) getChildFragmentManager().findFragmentById(R.id.fragment_container_node_list);
    }

}
