/*
 * Copyright (c) 2018 wmj
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the “Software”), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software
 * is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT
 * OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

package wmj.listacompras.ui.collection;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import wmj.listacompras.R;
import wmj.listacompras.db.entities.ChildCategory;
import wmj.listacompras.db.entities.ElementInfo;
import wmj.listacompras.viewmodel.CollectionViewModel;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link Listener}
 * interface.
 */
public class CollectionFragment extends Fragment {

    // parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";

    private int mColumnCount = 1;
    private ChildCategory mCategory;
    private Listener mListener;

    private CollectionViewModel mNodesModel;
    private CollectionRecyclerViewAdapter mAdapter;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public CollectionFragment() {}

    //@SuppressWarnings("unused")
    public static CollectionFragment newInstance(int columnCount) {
        CollectionFragment fragment = new CollectionFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
    }

    @Override
    public View onCreateView(
        LayoutInflater inflater,
        ViewGroup container,
        Bundle savedInstanceState
    ) {
        final View view = inflater.inflate(R.layout.fragment_collection, container, false);

        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view;
            if (mColumnCount <= 1) {
                recyclerView.setLayoutManager(new LinearLayoutManager(context));
            } else {
                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
            }

            mAdapter = new CollectionRecyclerViewAdapter(mListener);

            recyclerView.setAdapter(mAdapter);
        }
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mNodesModel = ViewModelProviders.of(getActivity()).get(CollectionViewModel.class);
        mNodesModel.getItems().observe(this, entities -> mAdapter.setItems(entities));
        mNodesModel.getCategory().observe(this, category -> mCategory = category);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try{
            mListener = (Listener) context;
        } catch(ClassCastException e) {
            throw new RuntimeException(context.toString()
                                           + " must implement CollectionFragment.Listener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void requestNewProduct(){
        if (mListener != null) {
            mListener.onNewProductRequested(mCategory.current.id);
        }
    }

    public void requestNewCategory(){
        if (mListener != null) {
            mListener.onNewCategoryRequested(mCategory.current.id);
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface Listener {
        void onProductSecondaryAction(View view, ElementInfo item);
        void onCategoryPrimaryAction(View view, ElementInfo item);
        void onCategorySecondaryAction(View view, ElementInfo item);
        void onProductSelected(View view, ElementInfo item, boolean selected);
        void onNewProductRequested(int parentCategoryId);
        void onNewCategoryRequested(int parentCategoryId);
        void onImport();
        void onExport();
    }
}
