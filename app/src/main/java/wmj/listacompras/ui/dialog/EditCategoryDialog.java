/*
 * Copyright (c) 2018 wmj
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the “Software”), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software
 * is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT
 * OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

package wmj.listacompras.ui.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import wmj.listacompras.R;

public class EditCategoryDialog extends DialogFragment {

    public interface EditCategoryDialogListener {
        void onEditCategoryDialogResult(String text);
    }

    private static final String ARG_NAME = "name";

    public static EditCategoryDialog newInstance(String name){
        EditCategoryDialog dialog = new EditCategoryDialog();
        Bundle args = new Bundle();
        args.putString(ARG_NAME, name);
        dialog.setArguments(args);
        return dialog;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();

        final View formView = inflater.inflate(R.layout.dialog_category_new, null);
        final EditText txtName = formView.findViewById(R.id.edit_category_name);
        txtName.setText(getArguments().getString(ARG_NAME));

        builder
            .setView(formView)
            .setTitle(R.string.edit_category)
            .setNegativeButton(android.R.string.cancel, null)
            .setPositiveButton(android.R.string.ok, (dialog, which) -> {
                final String name = txtName.getText().toString().trim();
                if(!name.isEmpty() && (mListener != null)){
                    mListener.onEditCategoryDialogResult(name);
                }
            });

        return builder.create();
    }

    EditCategoryDialogListener mListener;

    public void setEditCategoryDialogListener(final EditCategoryDialogListener listener) {
        mListener = listener;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
}
