/*
 * Copyright (c) 2018 wmj
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the “Software”), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software
 * is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT
 * OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

package wmj.listacompras.ui.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import wmj.listacompras.R;

public class EditShopListDialog extends DialogFragment {

    public interface EditShopListDialogListener {
        void onEditShopListDialogResult(String text);
    }

    private static final String ARG_NAME = "name";

    public static EditShopListDialog newInstance(String name) {
        EditShopListDialog dialog = new EditShopListDialog();
        Bundle args = new Bundle();
        args.putString(ARG_NAME, name);
        dialog.setArguments(args);
        return dialog;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();

        final View formView = inflater.inflate(R.layout.dialog_shoplist_edit, null);
        final EditText txtName = formView.findViewById(R.id.edit_shoplist_name);
        txtName.setText(getArguments().getString(ARG_NAME));

        builder
                .setView(formView)
                .setTitle(R.string.edit_shoplist)
                .setNegativeButton(android.R.string.cancel, null)
                .setPositiveButton(android.R.string.ok, (dialog, which) -> {
                    final String name = txtName.getText().toString().trim();
                    if(!name.isEmpty() && (mListener != null)){
                        mListener.onEditShopListDialogResult(name);
                    }
                });

        return builder.create();
    }

    EditShopListDialogListener mListener;

    public void setEditShopListDialogListener(final EditShopListDialogListener listener) {
        mListener = listener;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
}
