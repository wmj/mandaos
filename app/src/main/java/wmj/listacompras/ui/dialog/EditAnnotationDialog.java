/*
 * Copyright (c) 2018 wmj
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the “Software”), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software
 * is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT
 * OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

package wmj.listacompras.ui.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import wmj.listacompras.R;
import wmj.listacompras.db.entities.AnnotationInfo;

public class EditAnnotationDialog extends DialogFragment {

    public interface EditAnnotationDialogListener {
        void onEditAnnotationDialogResult(int productId, String details);
    }

    private static final String ARG_PRODUCT_ID = "product_id";
    private static final String ARG_PRODUCT_NAME = "product_name";
    private static final String ARG_CATEGORY_NAME = "category_name";
    private static final String ARG_DETAILS = "details";

    public static EditAnnotationDialog newInstance(AnnotationInfo item) {
        EditAnnotationDialog dialog = new EditAnnotationDialog();
        Bundle args = new Bundle();
        args.putInt(ARG_PRODUCT_ID, item.annotation.productId);
        args.putString(ARG_PRODUCT_NAME, item.name);
        args.putString(ARG_CATEGORY_NAME, item.categoryName);
        args.putString(ARG_DETAILS, item.annotation.details);
        dialog.setArguments(args);
        return dialog;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        final LayoutInflater inflater = getActivity().getLayoutInflater();

        final View formView = inflater.inflate(R.layout.dialog_annotation_edit, null);
        final EditText txtDetails = formView.findViewById(R.id.details);

        final Bundle arguments = getArguments();
        final int productId = arguments.getInt(ARG_PRODUCT_ID);
        txtDetails.setText(arguments.getString(ARG_DETAILS));

        final String title = String.format("%s (%s)",
            arguments.getString(ARG_PRODUCT_NAME),
            arguments.getString(ARG_CATEGORY_NAME)
        );

        builder
            .setView(formView)
            .setTitle(title)
            .setNegativeButton(android.R.string.cancel, null)
            .setPositiveButton(android.R.string.ok, (dialog, which) -> {
                String details = txtDetails.getText().toString().trim();
                if(details.isEmpty()){
                    details = null;
                }
                if(null != mListener){
                    mListener.onEditAnnotationDialogResult(productId, details);
                }
            });

        return builder.create();
    }

    EditAnnotationDialogListener mListener;

    public void setEditAnnotationDialogListener(final EditAnnotationDialogListener listener) {
        mListener = listener;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
}
