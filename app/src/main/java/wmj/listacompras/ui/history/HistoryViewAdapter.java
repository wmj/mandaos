/*
 * Copyright (c) 2018 wmj
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the “Software”), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software
 * is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT
 * OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

package wmj.listacompras.ui.history;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Comparator;
import java.util.List;

import wmj.listacompras.R;
import wmj.listacompras.db.entities.ElementWithCategory;
import wmj.listacompras.db.entities.table.CategoryEntity;
import wmj.listacompras.ui.listrecyclerview.AViewHolder;
import wmj.listacompras.ui.listrecyclerview.ListRecyclerViewAdapter;

class HistoryViewAdapter extends
    ListRecyclerViewAdapter<ElementWithCategory, HistoryViewAdapter.NodeViewHolder, HistoryFragment.Listener> {

    public HistoryViewAdapter(HistoryFragment.Listener listener) {
        super(listener);
    }

    @NonNull
    @Override
    public NodeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final View view = inflater.inflate(R.layout.fragment_history_item, parent, false);
        return new NodeViewHolder(view);
    }

    @Override
    public void setItems(List<ElementWithCategory> items) {
        int max = items.stream()
                .map(x -> x.frequency)
                .max(Comparator.comparing(Integer::valueOf))
                .orElse(1);
        for (final ElementWithCategory x : items) {
            x.frequency = (int) Math.round(3 * x.frequency / (double) max);
        }
        super.setItems(items);
    }

//region ViewHolder

    enum Frequency {
        LOW(R.drawable.ic_breezeicons_actions_22_view_statistics_l),
        MEDIUM(R.drawable.ic_breezeicons_actions_22_view_statistics_m),
        HIGH(R.drawable.ic_breezeicons_actions_22_view_statistics_h);

        public final int resource_id;
        Frequency(int id){
            resource_id = id;
        }

        public static  Frequency newInstance(int frequency) {
            if(frequency >= 3){
                return HIGH;
            }else if(frequency >= 2){
                return MEDIUM;
            }else{
                return LOW;
            }
        }
    }

    public class NodeViewHolder
        extends AViewHolder<ElementWithCategory>
        implements
            CompoundButton.OnCheckedChangeListener,
            View.OnLongClickListener
    {

        public ElementWithCategory mItem;
        public final View mView;
        public final CheckBox mContentView;
        public final TextView mCategoryView;
        public final ImageView mImageView;

        public NodeViewHolder(@NonNull View view) {
            super(view);
            mView = view;
            mContentView = view.findViewById(R.id.content);
            mCategoryView = view.findViewById(R.id.category);
            mImageView = view.findViewById(R.id.frequency);
        }

        @Override
        public void bind(@NonNull final ElementWithCategory item, int position) {
            mItem = item;
            mContentView.setText(item.name);
            setChecked(item.annotated);

            if(item.categoryId != CategoryEntity.ROOT_CATEGORY_ID){
                mCategoryView.setText(item.categoryName);
                mCategoryView.setVisibility(View.VISIBLE);
            }else{
                mCategoryView.setVisibility(View.GONE);
            }

            final int drawableId = Frequency.newInstance(item.frequency).resource_id;
            mImageView.setImageDrawable(mView.getContext().getDrawable(drawableId));

            mView.setOnLongClickListener(this);
            mContentView.setOnLongClickListener(this);
        }

        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if(null != mListener) {
                mListener.onHistoryProductSelected(buttonView, mItem, isChecked);
            }
        }

        private void setChecked(boolean value) {
            mContentView.setOnCheckedChangeListener(null);
            mContentView.setChecked(value);
            mContentView.setOnCheckedChangeListener(this);
        }

        @Override
        public boolean onLongClick(View view) {
            if(null != mListener) {
                mListener.onHistoryProductSecondaryAction(view, mItem);
            }
            return true;
        }
    }

    //endregion
}
