/*
 * Copyright (c) 2018 wmj
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the “Software”), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software
 * is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT
 * OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

package wmj.listacompras.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.view.MenuCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

import wmj.listacompras.R;
import wmj.listacompras.db.entities.AnnotationInfo;
import wmj.listacompras.db.entities.ChildCategory;
import wmj.listacompras.db.entities.ElementInfo;
import wmj.listacompras.db.entities.table.AnnotationEntity;
import wmj.listacompras.db.entities.table.CategoryEntity;
import wmj.listacompras.db.entities.table.ProductEntity;
import wmj.listacompras.db.entities.table.ShopListEntity;
import wmj.listacompras.file.ShopListFile;
import wmj.listacompras.ui.collection.CollectionFragment;
import wmj.listacompras.ui.collection.CollectionHeaderFragment;
import wmj.listacompras.ui.dialog.EditAnnotationDialog;
import wmj.listacompras.ui.dialog.EditCategoryDialog;
import wmj.listacompras.ui.dialog.EditProductDialog;
import wmj.listacompras.ui.dialog.NewCategoryDialog;
import wmj.listacompras.ui.dialog.NewProductDialog;
import wmj.listacompras.ui.history.HistoryFragment;
import wmj.listacompras.ui.shoplist.ShopListFragment;
import wmj.listacompras.viewmodel.BadgeViewModel;
import wmj.listacompras.viewmodel.CollectionViewModel;
import wmj.listacompras.viewmodel.HistoryViewModel;
import wmj.listacompras.viewmodel.ShopListViewModel;
import wmj.listacompras.viewmodel.ShopListsViewModel;

public class LauncherActivity extends AppCompatActivity
implements
    ShopListFragment.Listener,
    CollectionFragment.Listener,
    CollectionHeaderFragment.Listener,
    HistoryFragment.Listener
{

    private final static int CREATE_EXPORT_FILE_REQUEST_CODE = 10;
    private final static int OPEN_IMPORT_FILE_REQUEST_CODE = 11;

    private LauncherPagerAdapter mPagerAdapter;

    private ViewPager mViewPager;
    private View mShopListTabView;
    private TextView mViewBadge;

    private ShopListsViewModel mListsModel;
    private BadgeViewModel mBadgeModel;
    private HistoryViewModel mHistoryModel;
    private ShopListViewModel mShopListModel;
    private CollectionViewModel mCollectionModel;

    //region Activity

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launcher);

        mViewPager = findViewById(R.id.view_pager);
        mPagerAdapter = new LauncherPagerAdapter(getSupportFragmentManager(), this);
        mViewPager.setAdapter(mPagerAdapter);

        final TabLayout tabLayout = findViewById(R.id.tab_layout);

        connectTabsWithPager(tabLayout, mViewPager);

        final Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mShopListTabView = mPagerAdapter.getShopListTabView();
        mViewBadge = mShopListTabView.findViewById(R.id.badge_counter);
        for(int i=0; i<mPagerAdapter.getCount(); ++i){
            final TabLayout.Tab tab = tabLayout.getTabAt(i);
            final CharSequence title = mPagerAdapter.getPageTitle(i);
            if(i == mPagerAdapter.ID_SHOP_LIST){
                tab.setCustomView(mShopListTabView);
                final TextView vText = mShopListTabView.findViewById(R.id.title);
                vText.setText(title);
            }else{
                tab.setText(title);
            }
        }

//        final ActionBar ab = getSupportActionBar();
//        ab.setDisplayHomeAsUpEnabled(true);

        // view models
        mListsModel = ViewModelProviders.of(this).get(ShopListsViewModel.class);
        mShopListModel = ViewModelProviders.of(this).get(ShopListViewModel.class);
        mHistoryModel = ViewModelProviders.of(this).get(HistoryViewModel.class);
        mBadgeModel = ViewModelProviders.of(this).get(BadgeViewModel.class);

        mCollectionModel = ViewModelProviders.of(this).get(CollectionViewModel.class);

        mBadgeModel.getItem().observe(this, item -> mViewBadge.setText(String.format("%d", item)));

//        initializeShopList();

        mListsModel.getSelected().observe(this, this::updateDependentModels);
    }

    @Override
    protected void onResume() {
        super.onResume();
        initializeShopList();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar, menu);
        MenuCompat.setGroupDividerEnabled(menu, true);

        final MenuItem selector = menu.findItem(R.id.menu_toolbar_shoplist_selector);
        final ShopListSelector shopListSelector = (ShopListSelector) MenuItemCompat.getActionProvider(selector);
        selector.setOnActionExpandListener(new MenuItem.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem menuItem) {
                ShopListEntity item = mListsModel.getSelected().getValue();
                shopListSelector.setSelectedItem(item);
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem menuItem) {
                return true;
            }
        });
        mListsModel.getItems().observe(this, items -> {
            shopListSelector.setItems(items.stream().map(x -> x.list).collect(Collectors.toList()));
            final LiveData<ShopListEntity> selected = mListsModel.getSelected();
            if(selected != null) {
                shopListSelector.setSelectedItem(selected.getValue());
            }
        });

        // view model
        shopListSelector.setCallback(this::updateShopListModel);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.menu_toolbar_settings:
                openSettings();
                return true;
            case R.id.menu_toolbar_shoplists_manage:
                openShoplistManager();
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void openSettings() {
        final Intent intent = new Intent(this, SettingsActivity.class);
        startActivity(intent);
    }

    private void connectTabsWithPager(
        @NonNull final TabLayout tabLayout,
        @NonNull final ViewPager viewPager
    ) {
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                tabLayout.getTabAt(position).select();
            }

            @Override
            public void onPageScrolled(int i, float v, int i1) {}

            @Override
            public void onPageScrollStateChanged(int i) {}
        });

        tabLayout.addOnTabSelectedListener(new TabLayout.BaseOnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {}

            @Override
            public void onTabReselected(TabLayout.Tab tab) {}
        });
    }

    @Override
    protected void onActivityResult(
        int requestCode,
        int resultCode,
        @Nullable Intent data
    ) {
        switch(requestCode) {
            case CREATE_EXPORT_FILE_REQUEST_CODE:
                if(resultCode == Activity.RESULT_OK) {
                    if(data != null) {
                        final Uri uri = data.getData();
                        exportCollection(uri);
                    }
                }
                break;
            case OPEN_IMPORT_FILE_REQUEST_CODE:
                if(resultCode == Activity.RESULT_OK) {
                    if(data != null) {
                        final Uri uri = data.getData();
                        confirmImportCollection(uri);
                    }
                }
                break;
        }
    }

    @Override
    public void onBackPressed(){
        final int position = mViewPager.getCurrentItem();
        if(mPagerAdapter.isCollectionShowing(position)){
            if(backToParent()){
                return;
            }
        }
        super.onBackPressed();
    }

    //endregion

    //region actions

    //region shoplist

    void openShoplistManager() {
        final Intent intent = new Intent(this, ShopListsActivity.class);
        startActivity(intent);
    }

    void initializeShopList() {
        final ShopListEntity defaultList = mListsModel.getDefaultShopList();

        if(defaultList == null) return;

        mListsModel.setSelected(defaultList.id);
//        updateDependentModels(defaultList);
    }

    private SharedPreferences getStatusPreferences() {
        return getApplicationContext().getSharedPreferences(
                    getString(R.string.preference_status),
                    Context.MODE_PRIVATE
            );
    }

    void updateDependentModels(ShopListEntity item) {
        if(item == null) return;
        int shopListId = item.id;
        setTitle(item.name);
        mBadgeModel.setSelector(shopListId);
        mHistoryModel.setSelector(shopListId);
        mShopListModel.setSelector(shopListId);
        mCollectionModel.setShopList(shopListId);
    }

    void updateShopListModel(ShopListEntity item) {
        if(item == null) return;

        SharedPreferences prefs = getStatusPreferences();
        prefs
            .edit()
            .putInt(getString(R.string.preference_status_shoplist_id), item.id)
            .apply();

        mListsModel.setSelected(item);
    }

    //endregion

    //region collection

    /**
     * get a file uri
     * @param createFile true for creation, false for reading
     */
    private void requestFile(final boolean createFile) {
        final String action = createFile ?
                                  Intent.ACTION_CREATE_DOCUMENT :
                                  Intent.ACTION_OPEN_DOCUMENT;
        final Intent intent = new Intent(action);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType(ShopListFile.MIME_TYPE);
        intent.putExtra(Intent.EXTRA_TITLE, "mandaos.json");
        final int requestCode = createFile ?
                                    CREATE_EXPORT_FILE_REQUEST_CODE :
                                    OPEN_IMPORT_FILE_REQUEST_CODE;
        startActivityForResult(intent, requestCode);
    }

    /**
     * Import collection from a file
     * @param uri
     */
    private void importCollection(@NonNull final Uri uri) {
        try {
            final IOException e = mCollectionModel.importCollection(uri).get();
            final View view = findViewById(R.id.root);
            if(null == e) {
                Snackbar
                    .make(view, R.string.import_success, Snackbar.LENGTH_SHORT)
                    .show();
            }else{
                Snackbar
                    .make(view, "error al importar: " + e.getLocalizedMessage(), Snackbar.LENGTH_SHORT)
                    .show();
            }
        } catch (ExecutionException e1) {
            e1.printStackTrace();
        } catch (InterruptedException e1) {
            e1.printStackTrace();
        }
    }

    /**
     * Confirm for collection import
     * @param uri
     */
    private void confirmImportCollection(@NonNull final Uri uri) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder
            .setTitle(R.string.import_nodelist)
            .setMessage(R.string.message_confirm_import_collection)
            .setNegativeButton(android.R.string.cancel, null)
            .setPositiveButton(android.R.string.ok, (dialog, which) -> importCollection(uri))
            .show();
    }

    /**
     * Export collection to a file
     * @param uri
     */
    private void exportCollection(@NonNull final Uri uri) {
        try {
            final IOException e = mCollectionModel.exportCollection(uri).get();
            final View view = findViewById(R.id.root);
            if(null == e) {
                Snackbar
                    .make(view, R.string.export_success, Snackbar.LENGTH_SHORT)
                    .show();
            }else{
                Snackbar
                    .make(view, "error al exportar: " + e.getLocalizedMessage(), Snackbar.LENGTH_SHORT)
                    .show();
            }
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    //endregion

    //region product

    /**
     * Show edit product dialog
     * @param productNode product node
     */
    private void editProduct(@NonNull final ElementInfo productNode) {
        try {
            final ProductEntity product = mCollectionModel.promiseProduct(productNode.id).get();
            EditProductDialog dialog = EditProductDialog.newInstance(product.name);
            dialog.setEditProductDialogListener(text -> {
                product.name = text;
                mCollectionModel.update(product);
            });
            dialog.show(getSupportFragmentManager(), "edit product");
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Show remove product dialog
     * @param productNode
     */
    private void deleteProduct(@NonNull final ElementInfo productNode) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder
            .setTitle(R.string.remove_product)
            .setMessage(getResources().getString(R.string.remove_product_message, productNode.name))
            .setNegativeButton(android.R.string.cancel, null)
            .setPositiveButton(android.R.string.ok, (dialog, which) -> mCollectionModel.deleteProduct(productNode.id))
            .show();
    }

    /**
     * reset the product frequency
     * @param item
     */
    private void resetProductFrequency(@NonNull final ElementInfo item) {
        mHistoryModel.resetFrequency(item.id);
    }

    //endregion

    //region category

    /**
     * Show edit category dialog
     * @param categoryNode
     */
    private void editCategory(@NonNull final ElementInfo categoryNode) {
        try {
            final CategoryEntity category = mCollectionModel.promiseCategory(categoryNode.id).get();
            EditCategoryDialog dialog = EditCategoryDialog.newInstance(category.name);
            dialog.setEditCategoryDialogListener(text -> {
                category.name = text;
                mCollectionModel.update(category);
            });
            dialog.show(getSupportFragmentManager(), "edit category");
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Show remove category dialog
     * @param categoryNode
     */
    private void deleteCategory(@NonNull final ElementInfo categoryNode) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder
            .setTitle(R.string.remove_category)
            .setMessage(getResources().getString(R.string.remove_category_message, categoryNode.name))
            .setNegativeButton(android.R.string.cancel, null)
            .setPositiveButton(android.R.string.ok, (dialog, which) -> mCollectionModel.deleteCategory(categoryNode.id))
            .show();
    }

    //endregion

    //region navigation

    /**
     * Set the category to be listed
     * @param categoryId
     * @return
     */
    private void changeLevel(int categoryId){
        mCollectionModel.setLevel(categoryId);
    }

    /**
     * Move the collection view back to the parent category
     * @return true if can move
     */
    private boolean backToParent() {
        final ChildCategory category = mCollectionModel.getCategory().getValue();
        if(category.current.isRoot()){
            return false;
        }
        changeLevel(category.current.parentId);
        return true;
    }

    //endregion

    //endregion

    //region ShopListFragment.Listener

    @Override
    public void onAnnotationSelected(View view, AnnotationInfo item, boolean selected) {
        final AnnotationEntity annotation = item.annotation;
        annotation.selected = selected;
        mShopListModel.use(annotation.productId, selected);
    }

    @Override
    public void onAnnotationSecondaryAction(final View view, final AnnotationInfo mItem) {
        EditAnnotationDialog dialog = EditAnnotationDialog.newInstance(mItem);
        dialog.setEditAnnotationDialogListener((productId, details) -> {
            mItem.annotation.details = details;
            mShopListModel.update(mItem.annotation);
        });
        dialog.show(getSupportFragmentManager(), "edit annotation");
    }

    @Override
    public void onRemoveOldAnnotations() {
        mShopListModel.removeSelected();
    }

    @Override
    public void onSendShopList() {
        final String message = mShopListModel.asText();
        final Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.putExtra(Intent.EXTRA_TEXT, message);
        intent.putExtra(Intent.EXTRA_TITLE, getString(R.string.app_name));
        intent.setType("text/plain");
        startActivity(intent);
    }

    //endregion

    //region CollectionFragment.Listener

    @Override
    public void onProductSecondaryAction(View view, ElementInfo item) {
        PopupMenu menu = new PopupMenu(getApplicationContext(), view);
        menu.inflate(R.menu.collection_item);
        menu.setOnMenuItemClickListener(menuItem -> {
            switch(menuItem.getItemId()){
                case R.id.menu_collection_item_edit:
                    editProduct(item);
                    break;
                case R.id.menu_collection_item_remove:
                    deleteProduct(item);
                    break;
            }
            return false;
        });
        menu.show();
    }

    @Override
    public void onCategoryPrimaryAction(View view, ElementInfo item) {
        changeLevel(item.id);
    }

    @Override
    public void onCategorySecondaryAction(View view, ElementInfo item) {
        PopupMenu menu = new PopupMenu(getApplicationContext(), view);
        menu.inflate(R.menu.collection_item);
        menu.setOnMenuItemClickListener(menuItem -> {
            switch(menuItem.getItemId()){
                case R.id.menu_collection_item_edit:
                    editCategory(item);
                    break;
                case R.id.menu_collection_item_remove:
                    deleteCategory(item);
                    break;
            }
            return false;
        });
        menu.show();
    }

    @Override
    public void onProductSelected(View view, ElementInfo item, boolean selected) {
        final int MSG = selected ? R.string.toast_added_to_list : R.string.toast_removed_from_list;
        Snackbar.make(
                view,
                getString(MSG, item.name),
                Snackbar.LENGTH_SHORT
        ).show();

        if(selected) {
            mShopListModel.addAnnotation(item);
        }else{
            mShopListModel.delete(item.id);
        }
    }

    @Override
    public void onNewProductRequested(int parentCategoryId) {
        NewProductDialog dialog = new NewProductDialog();
        dialog.setNewProductDialogListener(text ->
            mCollectionModel.insertOrIgnore(new ProductEntity() {{
                name = text;
                categoryId = parentCategoryId;
            }})
        );
        dialog.show(getSupportFragmentManager(), "new product");
    }

    @Override
    public void onNewCategoryRequested(int parentCategoryId) {
        NewCategoryDialog dialog = new NewCategoryDialog();
        dialog.setNewCategoryDialogListener(text ->
            mCollectionModel.insertOrIgnore(new CategoryEntity() {{
                name = text;
                parentId = parentCategoryId;
            }})
        );
        dialog.show(getSupportFragmentManager(), "new category");
    }

    @Override
    public void onImport() {
        requestFile(false);
    }

    @Override
    public void onExport() {
        requestFile(true);
    }

    //endregion

    //region CollectionHeaderFragment.Listener

    @Override
    public void onReturnToParentCategory(ChildCategory currentCategory) {
        changeLevel(currentCategory.current.parentId);
    }

    //endregion

    //region HistoryFragment.Listener

    @Override
    public void onHistoryProductSecondaryAction(View view, ElementInfo item) {
        PopupMenu menu = new PopupMenu(getApplicationContext(), view);
        menu.inflate(R.menu.history_item);
        menu.setOnMenuItemClickListener(menuItem -> {
            switch(menuItem.getItemId()){
                case R.id.menu_history_items_forget:
                    resetProductFrequency(item);
                    break;
            }
            return false;
        });
        menu.show();
    }

    @Override
    public void onHistoryProductSelected(View view, ElementInfo item, boolean selected) {
        onProductSelected(view, item, selected);
    }

    @Override
    public void onHistoryReset() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder
            .setTitle(R.string.reset_history_title)
            .setMessage(R.string.reset_history)
            .setNegativeButton(android.R.string.cancel, null)
            .setPositiveButton(android.R.string.ok, (dialog, which) -> mHistoryModel.clear())
            .show();
    }

    //endregion
}
