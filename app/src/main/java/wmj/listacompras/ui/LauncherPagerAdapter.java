/*
 * Copyright (c) 2018 wmj
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the “Software”), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software
 * is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT
 * OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

package wmj.listacompras.ui;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.LayoutInflater;
import android.view.View;

import wmj.listacompras.R;
import wmj.listacompras.ui.collection.CollectionWrapperFragment;
import wmj.listacompras.ui.history.HistoryFragment;
import wmj.listacompras.ui.shoplist.ShopListFragment;

public class LauncherPagerAdapter extends FragmentPagerAdapter {

    private final static int N_TABS = 3;

    public final static int ID_SHOP_LIST = 0;
    public final static int ID_COLLECTION = 1;
    public final static int ID_HISTORY = 2;

    private final Context mContext;

    public LauncherPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        mContext = context;
    }

    @Override
    public Fragment getItem(int position) {
        switch(position) {
            case ID_SHOP_LIST:
                return getShopListFragment();
            case ID_COLLECTION:
                return getCollectionFragment();
            case ID_HISTORY:
                return getHistoryFragment();
        }
        return null;
    }

    public HistoryFragment getHistoryFragment() {
        return HistoryFragment.newInstance(1);
    }

    public CollectionWrapperFragment getCollectionFragment() {
        return new CollectionWrapperFragment();
    }

    public ShopListFragment getShopListFragment(){
        return ShopListFragment.newInstance(1);
    }

    @Override
    public int getCount() {
        return N_TABS;
    }

//    @Override
//    public int getItemPosition(Object object) {
//        if(mFragments.contains(object)){
//            return POSITION_UNCHANGED;
//        }
//        return POSITION_NONE;
//    }

    public boolean isCollectionShowing(int position) {
        return position == ID_COLLECTION;
    }

    public View getShopListTabView() {
        final View v = LayoutInflater.from(mContext).inflate(R.layout.tabitem_shoplist, null);
        return v;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        int idString;
        switch(position) {
            case ID_SHOP_LIST:
                idString = R.string.shop_list;
                break;
            case ID_COLLECTION:
                idString = R.string.items;
                break;
//            case ID_HISTORY:
            default:
                idString = R.string.history;
                break;
        }
        return mContext.getString(idString);
    }
}
