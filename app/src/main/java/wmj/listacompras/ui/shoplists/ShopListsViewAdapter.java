/*
 * Copyright (c) 2018 wmj
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the “Software”), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software
 * is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT
 * OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

package wmj.listacompras.ui.shoplists;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import java.text.DateFormat;
import java.util.Date;

import wmj.listacompras.R;
import wmj.listacompras.db.ShopListInfo;
import wmj.listacompras.db.entities.table.ShopListEntity;
import wmj.listacompras.ui.listrecyclerview.AViewHolder;
import wmj.listacompras.ui.listrecyclerview.ListRecyclerViewAdapter;

public class ShopListsViewAdapter extends
        ListRecyclerViewAdapter<ShopListInfo, ShopListsViewAdapter.ViewHolder, ShopListsFragment.Listener>
{

    public ShopListsViewAdapter(ShopListsFragment.Listener listener) {
        super(listener);
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View view = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.fragment_shoplists_item, parent, false);
        return new ViewHolder(view);
    }

    public static DateFormat dateFormat = DateFormat.getDateTimeInstance();

    public class ViewHolder extends AViewHolder<ShopListInfo> {


        public final View mView;
        public final TextView mNameView;
        public final TextView mBadgeView;
        public final TextView mTimestampView;
        public final ImageButton mDeleteView;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mNameView = view.findViewById(R.id.name);
            mDeleteView = view.findViewById(R.id.delete);
            mBadgeView = view.findViewById(R.id.badge_counter);
            mTimestampView = view.findViewById(R.id.timestamp);
        }

        @Override
        public void bind(@NonNull ShopListInfo item, int position) {
            mNameView.setText(item.list.name);
            mBadgeView.setText(String.format("%d", item.getCardinal()));

            final Date date = item.getLastModifiedAt();
            if(date != null) {
                mTimestampView.setVisibility(View.VISIBLE);
                mTimestampView.setText(dateFormat.format(date));
            }else{
                mTimestampView.setVisibility(View.GONE);
            }

            mView.setOnClickListener(view -> mListener.onEditShopList(view, item.list));

            final boolean canRemove = getItemCount() > 1;
            if(canRemove) {
                mDeleteView.setVisibility(View.VISIBLE);
                mDeleteView.setOnClickListener(view -> mListener.onDeleteShopList(view, item.list));
            }else{
                mDeleteView.setVisibility(View.INVISIBLE);
                mDeleteView.setOnClickListener(null);
            }
        }

    }
}
