/*
 * Copyright (c) 2018 wmj
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the “Software”), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software
 * is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT
 * OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

package wmj.listacompras.ui;

import android.arch.lifecycle.MutableLiveData;
import android.content.Context;
import android.support.v4.view.ActionProvider;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

import wmj.listacompras.R;
import wmj.listacompras.db.entities.table.ShopListEntity;

public class ShopListSelector extends ActionProvider implements AdapterView.OnItemSelectedListener {

    private final Context mContext;
    private final ArrayAdapter<ShopListEntity> mAdapter;
    private ShopListSelectorCallback mListener;
    private Spinner mSpinner;
    private SparseArray<Integer> mValues;

    private MutableLiveData<Integer> mSelected = new MutableLiveData<>();

    public ShopListSelector(Context context) {
        super(context);
        mContext = context;
        mAdapter = new ArrayAdapter<>(
                mContext,
                R.layout.shop_list_selector_item,
                R.id.title
        );
    }

    @Override
    public View onCreateActionView() {
        final LayoutInflater inflater = LayoutInflater.from(mContext);
        final View view = inflater.inflate(R.layout.shop_list_selector, null);
        mSpinner = view.findViewById(R.id.shoplist_selector);
        mSpinner.setAdapter(mAdapter);
        mSpinner.setOnItemSelectedListener(this);
        return view;
    }

    int nada() {
        return 1;
    }

    public void setItems(List<ShopListEntity> items) {
        mValues = new SparseArray<>();
        int idx = 0;
        for(final ShopListEntity item : items) {
            mValues.put(item.id, idx++);
        }
        if(mAdapter != null) {
            mAdapter.clear();
            mAdapter.addAll(items);
        }
    }

    public void setCallback(ShopListSelectorCallback callback) {
        mListener = callback;
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long rowId) {
        if(mListener != null){
            mListener.onShopListSelected(mAdapter.getItem(position));
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
        nada();
    }

    public interface ShopListSelectorCallback {
        void onShopListSelected(ShopListEntity shopList);
    }

    public void setSelectedItem(ShopListEntity item) {
        if(mValues == null) return;
        int position = Optional.ofNullable(item)
                .map(x -> mValues.get(x.id, -1))
                .orElse(-1);
//        int position = mValues.get(item.id, -1);
        mSelected.setValue(position);
        if(mSpinner != null) {
            mSpinner.setOnItemSelectedListener(null);
            mSpinner.setSelection(position >= 0 ? position : 0);
            mSpinner.setOnItemSelectedListener(this);
        }
    }
}
