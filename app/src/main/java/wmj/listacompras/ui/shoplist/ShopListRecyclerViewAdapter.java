/*
 * Copyright (c) 2018 wmj
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the “Software”), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software
 * is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT
 * OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

package wmj.listacompras.ui.shoplist;

import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import wmj.listacompras.R;
import wmj.listacompras.db.entities.AnnotationInfo;
import wmj.listacompras.db.entities.table.CategoryEntity;
import wmj.listacompras.ui.listrecyclerview.AViewHolder;
import wmj.listacompras.ui.listrecyclerview.ListRecyclerViewAdapter;

/**
 * {@link RecyclerView.Adapter} that can display a {@link AnnotationInfo} and makes a call to the
 * specified {@link ShopListFragment.Listener}.
 */
public class ShopListRecyclerViewAdapter extends
    ListRecyclerViewAdapter<AnnotationInfo, ShopListRecyclerViewAdapter.ExtendedAnnotationViewHolder, ShopListFragment.Listener>
{

    enum ItemType {
        SELECTED,
        UNSELECTED,
        CATEGORY,
    }

    public ShopListRecyclerViewAdapter(ShopListFragment.Listener listener) {
        super(listener);
    }

    @Override
    public ExtendedAnnotationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemType type = ItemType.values()[viewType];
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view;
        switch(type) {
            case UNSELECTED:
                view = inflater.inflate(R.layout.fragment_shoplist_annotation, parent, false);
                return new UnselectedAnnotationViewHolder(view);
            case SELECTED:
                view = inflater.inflate(R.layout.fragment_shoplist_annotation_selected, parent, false);
                return new SelectedAnnotationViewHolder(view);
            default:
                view = inflater.inflate(R.layout.fragment_shoplist_category, parent, false);
                return new CategoryViewHolder(view);
        }
    }

    @Override
    public void setItems(final List<AnnotationInfo> items) {
        super.setItems(transform(items));
    }

    private List<AnnotationInfo> transform(final List<AnnotationInfo> original) {
        final List<AnnotationInfo> out = new ArrayList<>();
        Integer lastCatId = null;
        for(final AnnotationInfo item: original) {
            final int catId = item.annotation.selected ? CategoryEntity.ORPHAN_CATEGORY_ID : item.categoryId;
            if((lastCatId == null) || (lastCatId != catId)) {
                final String catName = item.annotation.selected ? null : item.categoryName;
                out.add(new AnnotationInfo() {{
                    categoryId = catId;
                    categoryName = catName;
                    annotation = null;
                }});
            }
            lastCatId = catId;
            out.add(item);
        }
        return out;
    }

    @Override
    public int getItemViewType(int position){
        final AnnotationInfo item = mValues.get(position);
        final ItemType type;
        if(item.annotation == null){
            type = ItemType.CATEGORY;
        }else{
            type = item.annotation.selected ? ItemType.SELECTED : ItemType.UNSELECTED;
        }

        return type.ordinal();
    }

    //region ViewHolder

    public abstract class ExtendedAnnotationViewHolder extends AViewHolder<AnnotationInfo> {
        public final View mView;

        public ExtendedAnnotationViewHolder(@NonNull View view) {
            super(view);
            mView = view;
        }
    }

    public class CategoryViewHolder extends ExtendedAnnotationViewHolder {

        public final TextView mCategoryView;

        public CategoryViewHolder(@NonNull View view) {
            super(view);
            mCategoryView = view.findViewById(R.id.category);
        }

        public void bind(@NonNull AnnotationInfo item, int position) {
            switch(item.categoryId){
                case CategoryEntity.ROOT_CATEGORY_ID:
                    mCategoryView.setText(R.string.not_classified);
                    break;
                case CategoryEntity.ORPHAN_CATEGORY_ID:
                    mCategoryView.setText(R.string.already_bought);
                    break;
                default:
                    mCategoryView.setText(item.categoryName);
            }
        }
    }

    public abstract class AnnotationViewHolder
        extends ExtendedAnnotationViewHolder
        implements
            CompoundButton.OnCheckedChangeListener,
            View.OnLongClickListener
    {

        public AnnotationInfo mItem;
        public final CheckBox mNameView;

        public AnnotationViewHolder(@NonNull View view) {
            super(view);
            mNameView = view.findViewById(R.id.product_name);
        }

        @CallSuper
        public void bind(@NonNull AnnotationInfo item, int position) {
            mItem = item;

            mNameView.setText(mItem.name);
            mNameView.setOnCheckedChangeListener(this);
            mView.setOnLongClickListener(this);
            mView.setOnClickListener(view -> {
                mNameView.setChecked(!mNameView.isChecked());
            });
            mNameView.setOnLongClickListener(this);
        }

        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if (null != mListener) {
                mListener.onAnnotationSelected(mView, mItem, isChecked);
            }
        }

        @Override
        public boolean onLongClick(View view) {
            if(null != mListener){
                mListener.onAnnotationSecondaryAction(view, mItem);
            }
            return true;
        }

        public void selectItem(boolean selected) {
            mNameView.setOnCheckedChangeListener(null);
            mNameView.setChecked(selected);
            mNameView.setOnCheckedChangeListener(this);
        }
    }

    public class UnselectedAnnotationViewHolder extends AnnotationViewHolder {
        public final TextView mDetailsView;

        public UnselectedAnnotationViewHolder(@NonNull View view) {
            super(view);
            mDetailsView = view.findViewById(R.id.product_details);
        }

        @Override
        public void bind(@NonNull final AnnotationInfo item, int position) {
            super.bind(item, position);

            // checkbox
            selectItem(false);
            // details
            if(item.annotation.details != null) {
                mDetailsView.setText(item.annotation.details);
                mDetailsView.setVisibility(View.VISIBLE);
            } else {
                mDetailsView.setVisibility(View.GONE);
            }
        }
    }

    public class SelectedAnnotationViewHolder extends AnnotationViewHolder {

        public SelectedAnnotationViewHolder(@NonNull View itemView) {
            super(itemView);
        }

        @Override
        public void bind(@NonNull final AnnotationInfo item, int position) {
            super.bind(item, position);
            selectItem(true);
            final TextView categoryName = mView.findViewById(R.id.category);
            if(item.categoryId == CategoryEntity.ROOT_CATEGORY_ID){
                categoryName.setText(null);
            }else {
                categoryName.setText(item.categoryName);
            }
        }
    }

    //endregion
}
