/*
 * Copyright (c) 2018 wmj
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the “Software”), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software
 * is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT
 * OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

package wmj.listacompras.viewmodel;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.ExecutionException;

import wmj.listacompras.db.entities.AnnotationInfo;
import wmj.listacompras.db.entities.ElementInfo;
import wmj.listacompras.db.entities.table.AnnotationEntity;
import wmj.listacompras.db.entities.table.ShopListEntity;
import wmj.listacompras.viewmodel.db.ListViewModel;

public class ShopListViewModel extends ListViewModel<AnnotationInfo, Integer> {

    public ShopListViewModel(@NonNull Application application) {
        super(application);
    }

    @Override
    protected LiveData<List<AnnotationInfo>> fetchItems(Integer shopListId) {
        return getRepository().listItems(shopListId);
    }

    private Integer getShopListId() {
        return Optional.ofNullable(getSelector())
                .map(x -> x.getValue())
                .orElse(null);
    }

    /**
     * remove selected annotations
     */
    public void removeSelected() {
        getRepository().removeOldAnnotations(getShopListId());
    }

    /**
     * select or unselect an annotation
     * @param productId
     * @param selected
     */
    public void use(int productId, boolean selected) {
        getRepository().useAnnotation(getShopListId(), productId, selected);
    }

    /**
     * update annotation
     * @param annotation
     */
    public void update(AnnotationEntity annotation) {
        getRepository().updateAnnotation(annotation);
    }

    public void delete(int productId) {
        getRepository().deleteAnnotation(getShopListId(), productId);
    }

    public void addAnnotation(ElementInfo item) {
        getRepository().addAnnotation(getShopListId(), item);
    }

    public String asText() {
        StringBuilder sb = new StringBuilder();
        Integer currentCategory = null;
        for(AnnotationInfo info: getItems().getValue()) {
            if (info.annotation.selected) continue;
            if ((currentCategory == null) || (info.categoryId != currentCategory)) {
                currentCategory = info.categoryId;
                sb.append("\n");
                sb.append(info.categoryName);
                sb.append("\n");
            }
            sb.append("* ");
            sb.append(info.name);
            if((info.annotation.details != null) && !info.annotation.details.isEmpty()) {
                sb.append(" (");
                sb.append(info.annotation.details);
                sb.append(")");
            }
            sb.append("\n");
        }
        return sb.toString().trim();
    }

    public void removeOldSelected(int time) {
        getRepository().removeAllOldAnnotations(time);
    }
}
