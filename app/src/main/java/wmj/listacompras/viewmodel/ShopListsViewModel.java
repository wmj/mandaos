/*
 * Copyright (c) 2018 wmj
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the “Software”), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software
 * is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT
 * OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

package wmj.listacompras.viewmodel;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;

import java.util.List;
import java.util.concurrent.ExecutionException;

import wmj.listacompras.db.ShopListInfo;
import wmj.listacompras.db.entities.table.ShopListEntity;
import wmj.listacompras.viewmodel.db.DBViewModel;

public class ShopListsViewModel extends DBViewModel {

    private final MutableLiveData<ShopListEntity> mSelected = new MutableLiveData<ShopListEntity>() {{
        setValue(getDefaultShopList());
    }};

    private final LiveData<List<ShopListInfo>> mItems;

    public ShopListsViewModel(@NonNull Application application) {
        super(application);
        mItems = getRepository().selectShopLists();
    }

    public LiveData<List<ShopListInfo>> getItems() {
        return mItems;
    }

    public LiveData<ShopListEntity> getSelected() {
        return mSelected;
    }

    public void setSelected(ShopListEntity value) {
        mSelected.setValue(value);
    }

    public void setSelected(int listId) {
        ShopListEntity item = null;
        try {
            item = getRepository().selectShopList(listId).get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if(item != null) {
            setSelected(item);
        }
    }

    public ShopListEntity getDefaultShopList() {
        return getRepository().getDefaultShopList(getApplication().getApplicationContext());
    }

    public void update(ShopListEntity shopList) {
        getRepository().updateShopList(shopList);
    }

    public void insert(ShopListEntity shopList) {
        getRepository().insertShopList(shopList);
    }

    public void deleteShopList(int shopListId) {
        getRepository().deleteShopList(shopListId);
    }
}
