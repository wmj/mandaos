/*
 * Copyright (c) 2018 wmj
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the “Software”), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software
 * is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT
 * OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

package wmj.listacompras.viewmodel;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.ExecutionException;

import wmj.listacompras.db.entities.ElementWithCategory;
import wmj.listacompras.db.entities.table.ShopListEntity;
import wmj.listacompras.viewmodel.db.ListViewModel;

public class HistoryViewModel extends ListViewModel<ElementWithCategory, Integer> {

    public HistoryViewModel(@NonNull Application application){
        super(application);
    }

    @Override
    protected LiveData<List<ElementWithCategory>> fetchItems(Integer selector) {
        return getRepository().listHistory(selector);
    }

    /**
     * Clear the history
     */
    public void clear() {
        getRepository().resetHistory();
    }

    /**
     * Set the product frequency to 0
     * @param productId
     */
    public void resetFrequency(int productId) {
        getRepository().resetProductFrequency(productId);
    }
}
