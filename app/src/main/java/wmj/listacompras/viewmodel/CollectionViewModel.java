/*
 * Copyright (c) 2018 wmj
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the “Software”), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software
 * is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT
 * OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

package wmj.listacompras.viewmodel;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.net.Uri;
import android.support.annotation.NonNull;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import wmj.listacompras.AppExecutors;
import wmj.listacompras.db.entities.ChildCategory;
import wmj.listacompras.db.entities.ElementInfo;
import wmj.listacompras.db.entities.table.CategoryEntity;
import wmj.listacompras.db.entities.table.ProductEntity;
import wmj.listacompras.db.entities.table.ShopListEntity;
import wmj.listacompras.viewmodel.db.DBViewModel;

public class CollectionViewModel extends DBViewModel {

    public CollectionViewModel(@NonNull Application application) {
        super(application);
    }


    //region data accessors
    public LiveData<ChildCategory> getCategory() { return mCategory; }

    public LiveData<List<ElementInfo>> getItems() { return mItems; }


    //endregion

    //region PRIVATE

    class Selector {
        public Integer parentId;
        public Integer listId;
    }

    private MutableLiveData<Integer> mParentId = new MutableLiveData<Integer>() {{
        setValue(CategoryEntity.ROOT_CATEGORY_ID);
    }};

    private MutableLiveData<Selector> mSelector = new MutableLiveData<>();

    private LiveData<ChildCategory> mCategory =
        Transformations.switchMap(mParentId, this::fetchCategory);

    private LiveData<List<ElementInfo>> mItems =
        Transformations.switchMap(mSelector, this::fetchNodes);

    private int normalizeLevel(Integer level) {
        if (level == null){
            return CategoryEntity.ROOT_CATEGORY_ID;
        }
        return level;
    }

    private LiveData<ChildCategory> fetchCategory(Integer categoryId) {
        return getRepository().selectCategoryWithParent(normalizeLevel(categoryId));
    }

    private LiveData<List<ElementInfo>> fetchNodes(Selector selector) {
        if(selector == null){
            return new LiveData<List<ElementInfo>>() {{

            }};
        }
        final int listId = selector.listId;
        final int catId = normalizeLevel(selector == null ? null : selector.parentId);
        return getRepository().listNodes(listId, catId);
    }

    //endregion

    //region ACTIONS

    /**
     * change the category level
     * @param levelId
     */
    public void setLevel(int levelId) {
        final Integer current = mParentId.getValue();
        if((current == null) || (current != levelId)) {
            mParentId.setValue(levelId);
            final Selector currentSelector = mSelector.getValue();
            if(currentSelector != null) {
                mSelector.setValue(new Selector() {{
                    parentId = levelId;
                    listId = currentSelector.listId;
                }});
            }
        }
    }

    public void setShopList(int shopListId) {
        final Selector current = mSelector.getValue();
        if((current == null) || (current.listId != shopListId)) {
            mSelector.setValue(new Selector() {{
                listId = shopListId;
                parentId = mParentId.getValue();
            }});
        }
    }

    /**
     * insert or ignore product
     * @param product
     */
    public void insertOrIgnore(ProductEntity product) {
        getRepository().insertOrIgnoreProduct(product);
    }

    /**
     * insert or ignore category
     * @param category
     */
    public void insertOrIgnore(CategoryEntity category) {
        getRepository().insertOrIgnoreCategory(category);
    }

    /**
     * update category
     * @param category
     */
    public void update(CategoryEntity category) {
        getRepository().updateCategory(category);
    }

    /**
     * update product
     * @param product
     */
    public void update(ProductEntity product) {
        getRepository().updateProduct(product);
    }

    /**
     * delete category by id
     * @param categoryId
     */
    public void deleteCategory(int categoryId) {
        getRepository().deleteCategory(categoryId);
    }

    /**
     * delete product by id
     * @param productId
     */
    public void deleteProduct(int productId) {
        getRepository().deleteProduct(productId);
    }

    /**
     * get a category in the future
     * @param categoryId
     * @return
     */
    public Future<CategoryEntity> promiseCategory(int categoryId) {
        return getRepository().selectFutureCategory(categoryId);
    }

    /**
     * get a promise in the future
     * @param productId
     * @return
     */
    public Future<ProductEntity> promiseProduct(int productId) {
        return getRepository().selectFutureProduct(productId);
    }

    /**
     * export collection to a file
     * @param uri
     * @return null if successful
     */
    public Future<IOException> exportCollection(@NonNull final Uri uri) {
        return AppExecutors.diskExecutor().submit(() -> {
            try(final OutputStream out = getApplication().getContentResolver().openOutputStream(uri)) {
                getRepository().exportDatabase(out);
                return null;
            } catch (IOException e) {
                return e;
            }
        });
    }

    /**
     * replace collection from a file
     * @param uri
     * @return null if successful
     */
    public Future<IOException> importCollection(@NonNull final Uri uri) {
        return AppExecutors.diskExecutor().submit(() -> {
            try(final InputStream in = getApplication().getContentResolver().openInputStream(uri)){
                getRepository().importDatabase(in);
                return null;
            } catch (IOException e) {
                return e;
            }
        });
    }

    //endregion
}
