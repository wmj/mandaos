/*
 * Copyright (c) 2018 wmj
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the “Software”), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software
 * is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT
 * OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

package wmj.listacompras.file;

import android.util.JsonWriter;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.util.List;

import wmj.listacompras.db.entities.Element;
import wmj.listacompras.db.entities.tree.Tree;

public class ShopListFileWriter implements AutoCloseable {

    final private JsonWriter mWriter;
    final private OutputStreamWriter mOstreamWriter;

    public ShopListFileWriter(final OutputStream out)
        throws UnsupportedEncodingException
    {
        mOstreamWriter = new OutputStreamWriter(out, "UTF-8");
        mWriter = new JsonWriter(mOstreamWriter);
        mWriter.setIndent("  ");
    }

    public void writeCategory(Tree<? extends Element> node) throws IOException {
        mWriter.beginObject();
        if(!node.products.isEmpty()){
            mWriter.name(ShopListFile.TOKEN_PRODUCTS);
            writeProducts(node.products);
        }
        if(!node.categories.isEmpty()){
            mWriter.name(ShopListFile.TOKEN_CATEGORIES);
            writeCategories(node.categories);
        }
        mWriter.endObject();
    }

    private void writeCategories(List<? extends Tree<? extends Element>> categories)
        throws IOException
    {
        mWriter.beginObject();
        for (final Tree<? extends Element> category : categories) {
            mWriter.name(category.item.name);
            writeCategory(category);
        }
        mWriter.endObject();
    }

    private void writeProducts(List<? extends Element> products) throws IOException {
        mWriter.beginArray();
        for (final Element product : products) {
            mWriter.value(product.name);
        }
        mWriter.endArray();
    }

    @Override
    public void close() throws IOException {
        mWriter.close();
        mOstreamWriter.close();
    }

}
