/*
 * Copyright (c) 2018 wmj
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the “Software”), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software
 * is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT
 * OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

package wmj.listacompras.file;

import android.support.annotation.NonNull;
import android.util.JsonReader;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import wmj.listacompras.db.entities.table.CategoryEntity;
import wmj.listacompras.db.entities.table.ProductEntity;


public class ShopListFileParser {

    private final Listener mListener;

    private int categoryCounter;

    public ShopListFileParser(final Listener listener) {
        mListener = listener;
    }

    public void parse(@NonNull final InputStream jsonStream) throws IOException {
        try(
            final InputStreamReader isr = new InputStreamReader(jsonStream, "UTF-8");
            final JsonReader reader = new JsonReader(isr)
        ){
            categoryCounter = 1;
            parseCategory(reader, CategoryEntity.rootCategory());
        }
    }

    private void parseCategory(
        @NonNull final JsonReader reader,
        @NonNull final CategoryEntity category
    ) throws IOException {
        reader.beginObject();
        emitCategory(category);
        while(reader.hasNext()){
            final String field = reader.nextName();
            switch(field) {
                case ShopListFile.TOKEN_PRODUCTS:
                    parseProducts(reader, category);
                    break;
                case ShopListFile.TOKEN_CATEGORIES:
                    parseCategories(reader, category);
                    break;
                default:
                    throw new IOException(String.format("Unexpected field '%s'", field));
            }
        }
        reader.endObject();
    }

    private void parseCategories(
        @NonNull final JsonReader reader,
        @NonNull final CategoryEntity parent
    ) throws IOException {
        reader.beginObject();
        while(reader.hasNext()){
            parseCategory(reader, new CategoryEntity(){{
                id = categoryCounter++;
                name = reader.nextName();
                parentId = parent.id;
            }});
        }
        reader.endObject();
    }

    private void parseProducts(
        @NonNull final JsonReader reader,
        @NonNull final CategoryEntity parent
    ) throws IOException {
        reader.beginArray();
        final List<ProductEntity> products = new ArrayList<>();
        while(reader.hasNext()){
            products.add(new ProductEntity() {{
                categoryId = parent.id;
                name = reader.nextString();
            }});
        }
        reader.endArray();
        emitProducts(products);
    }

    private void emitProducts(@NonNull final List<ProductEntity> products) {
        if(null != mListener){
            mListener.onNewProductList(products);
        }
    }

    private void emitCategory(@NonNull final CategoryEntity category) {
        if(null != mListener){
            mListener.onNewCategory(category);
        }
    }

    public interface Listener {
        void onNewCategory(@NonNull CategoryEntity category);
        void onNewProductList(@NonNull List<ProductEntity> products);
    }
}
